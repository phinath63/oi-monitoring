<?php
return [
    'dashboard' => 'Dashboard',
    'system-config' => 'System Configure',
    'system' => [
        'table' => 'តរាង',
        'category' => 'Categories',
        'condition' => 'Conditions',
        'language' => 'Languages',
        'entity-defined-field-condition' => 'Entity Defined Field Condition',
        'entity-defined-field' => 'Entity Defined Fields',
        'entity-defined-field-value' => 'Entity Defined Field Value',
        'entity-defined-field-search' => 'Entity Defined Field Search'
    ],
    'data-monitor' => 'Monitoring',
    'monitor'=>[
        'information-list'=>'Information List',
        'data-list'=>'Data List',
        'information-editing'=>'Information Edit'
    ]
];
