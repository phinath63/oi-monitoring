var entity = angular.module("entity", []);
entity.controller("entityInfoController", function ($scope, $http) {
//    $scope.categories = [
//        {name: 'abc',processing:0,;
//            options: [{conjunction :'and',key: {udfType: 1, keyValue: ''}, condition: '>', value: 456}]
//        }
//    ];
    $scope.selections = [];
    $scope.geography = {};
    $scope.geography.type = 'country';
    $scope.categories = [];
    $scope.addCat = function (name) {

        $scope.categories.push({name: name, processing: 0, options: []});
    };
    $scope.addOption = function (catIndex, conjunction) {
        $scope.categories[catIndex].options.push({conjunction: conjunction, key: {udfType: 1, keyValue: ''}, conditions: [], condition: '', listValues: [], value: ''});
    };
    $scope.removeOption = function (catIndex, optionIndex) {
        $scope.categories[catIndex].options.splice(optionIndex, 1);
    };
    $scope.loadValue = function (catid, optionid) {
        $scope.categories[catid].options[optionid].listValues = [];
        $http({
            method: 'GET',
            url: "monitor/entity-info-field/" + $scope.categories[catid].options[optionid].key.keyValue
        }).then(function (response) {
            if (response.data.values.length === 0) {
                $scope.categories[catid].options[optionid].key.edfSearchType = 2;
            } else {
                $scope.categories[catid].options[optionid].key.edfSearchType = 1;
            }
            $scope.categories[catid].options[optionid].listValues = response.data.values;
            $scope.categories[catid].options[optionid].conditions = response.data.conditions;
        }, function (response) {

        });
    };
    $scope.view = function () {
        //make the categori
        var conditions = [];
        for (i = 0; i < $scope.categories.length; i++) {
            for (j = 0; j < $scope.categories[i].options.length; j++) {
                conditions.push({conjunction: $scope.categories[i].options[j].conjunction,
                    keyValue: $scope.categories[i].options[j].key.keyValue,
                    condition: $scope.categories[i].options[j].condition,
                    value: $scope.categories[i].options[j].value});
            }
        }
        var checkboxes = document.getElementsByClassName("selections");
        var selections = [];
        // loop over them all
        for (var i = 0; i < checkboxes.length; i++) {
            // And stick the checked ones onto an array...
            if (checkboxes[i].checked) {
                selections.push(checkboxes[i].value);
            }
        }
        $http({
            method: 'GET',
            url: "monitor/entity-info",
            params: {gp_type: $scope.geography.type,
                gp_province: $scope.geography.province,
                gp_district: $scope.geography.district,
                gp_commune: $scope.geography.commune,
                gp_village: $scope.geography.village,
                data: JSON.stringify(conditions),
                selections: JSON.stringify(selections)
            },
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function (response) {
            document.getElementById("form-result").innerHTML = response.data;
            reloadScript();
        });
    };
    $scope.reset = function () {
        $scope.selections = [];
        $scope.geography = {};
        $scope.geography.type = 'country';
        for (i = 0; i < $scope.categories.length; i++) {
            $scope.categories[i].options = [];
            $scope.categories[i].selectedField = true;
        }
    };

});
