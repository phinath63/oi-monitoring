<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::group(['middleware' => ['web'], 'as' => 'auth::', 'prefix' => 'auth', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', function () {
            return view('auth.login');
        }]);
    Route::post('process', ['as' => 'process', 'uses' => 'LoginController@process']);
});

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => ['web']], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('/dashboard', ['as' => 'dashboard', function() {
            return view('content.dashboard');
        }]
    );
    Route::resource('system/table', 'TableController');
    Route::resource('system/condition', 'ConditionController');
    Route::resource('system/language', 'LanguageController');
    Route::resource('system/category', 'EntityDefinedCategoryController');
    Route::get('system/entity-field/getColumnAndCategoryName/{id}', 'EntityDefinedFieldController@getColumnAndCategoryName');
    Route::resource('system/entity-field-condition', 'EntityDefinedFieldConditionController');
    Route::resource('system/entity-field', 'EntityDefinedFieldController');
    Route::resource('system/entity-field-search', 'EntityDefinedSearchController');

    // change language
    Route::get('system/language/change-language/{lang}', function($lang) {
        App::setLocale($lang);
        echo 'worki';
    });
    // monitoring

    Route::get('monitor/entity-info-field/{fieldID}', 'InformationController@showFieldListValue'); // show list value
    // Route::get('monitor/entity-info/{tableID}/{category}', 'InformationController@showFieldRelatedWithCategory');
    Route::resource('monitor/entity-info', 'InformationController');
    Route::get('PDCV/{type}/{code}', 'PDCVController@getLocation');
});
