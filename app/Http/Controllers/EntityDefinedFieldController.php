<?php

namespace MONITORING\Http\Controllers;

use Illuminate\Http\Request;
use MONITORING\Http\Requests;
use MONITORING\Http\Controllers\Controller;
use DB;
use MONITORING\EntityDefinedField;
use MONITORING\EntityDefinedFieldList;
use MONITORING\Table;
use MONITORING\EntityDefinedCategory;

class EntityDefinedFieldController extends Controller {

    public function index() {
        $tables = Table::all();
        return response()
                        ->view('content.system.entity_defined_field', ['tables' => $tables]);
    }

    public function show($id) { //table id
        $entity_defined_fields = DB::table('entitydefinedfieldwithlist')
                ->where('TableID', '=', $id)
                ->get();
        return response($entity_defined_fields, 200);
    }

    public function getColumnAndCategoryName($id) {
        $field['columns'] = DB::table('INFORMATION_SCHEMA.COLUMNS')
                ->select('COLUMN_NAME')
                ->where([['TABLE_SCHEMA', '=', 'dynamicfield'],
                    ['TABLE_NAME', '=', Table::find($id)->TableName]])
                ->whereNotIn('COLUMN_NAME', ['created_at', 'updated_at'])
                ->get();
        $field['categories'] = DB::table('entitydefinedcategory')
                ->select('EntityDefinedCategoryCode', 'EntityDefinedCategoryName')
                ->where([['TableID', $id], ['LanguageID', 1]])
                ->get();
        return response($field, 200);
    }

    public function destroy($id) {
        DB::table('entitydefinedfield')
                ->where('EntityDefinedFieldListCode', '=', $id)
                ->delete();
        DB::table('entitydefinedfieldlist')
                ->where('EntityDefinedFieldListCode', '=', $id)
                ->delete();
        return response('Your field has been deleted', 200);
    }

    public function store(Request $request) {
        // problem is list code :(
// find last code id

        $lastFieldCode = DB::table('entitydefinedfieldlist')
                ->select('EntityDefinedFieldListCode')
                ->orderBy('EntityDefinedFieldListCode', 'desc')
                ->first();
        if ($lastFieldCode === NULL) {
            $lastFieldCode = 1;
        } else {
            $lastFieldCode = $lastFieldCode->EntityDefinedFieldListCode + 1;
        }
        try {
            DB::beginTransaction();
            $entityList = New EntityDefinedFieldList;
            $entityList->EntityDefinedFieldListCode = $lastFieldCode;
            $entityList->EntityDefinedFieldListName = $request->input('fieldNameLatin');
            $entityList->LanguageID = 1;
            $entityList->save();

            $entityListKh = New EntityDefinedFieldList;
            $entityListKh->EntityDefinedFieldListCode = $lastFieldCode;
            $entityListKh->EntityDefinedFieldListName = $request->input('fieldNameKh');
            $entityListKh->LanguageID = 2;
            $entityListKh->save();

            $entity = new EntityDefinedField;
            $entity->TableID = $request->input('tableID');
            $entity->EntityDefinedCategoryCode = $request->input('catCode');
            $entity->EntityDefinedFieldListCode = $lastFieldCode;
            $entity->EntityDefinedFieldNameInTable = $request->input('fieldInTable');
            $entity->EDFSearchType = $request->input('edfSearchType');
            $entity->EDFType = $request->input('edfType');
            $entity->DisplayField = $request->input('displayField');
            $entity->save();
            DB::commit();

            $category = EntityDefinedCategory::where('EntityDefinedCategoryCode', $_POST['catCode'])
                    ->orderBy('LanguageID', 'asc')
                    ->get();
            $response['EntityDefinedFieldNameInTable'] = $request->input('fieldInTable');
            $response['EntityDefinedFieldListCode'] = $lastFieldCode;
            $response['EntityDefinedCategoryNameEN'] = $category[0]->EntityDefinedCategoryName;
            $response['EntityDefinedCategoryNameKH'] = $category[1]->EntityDefinedCategoryName;
            $response['EntityDefinedFiledListNameEN'] = $request->input('fieldNameLatin');
            $response['EntityDefinedFiledListNameKH'] = $request->input('fieldNameKh');
            $response['EDFSearchType'] = $request->input('edfSearchType');
            $response['EDFType'] = $request->input('edfType');
            $response['DisplayField'] = $request->input('displayField');
            return response($response, 200);
        } catch (Exception $ex) {
            DB::rollBack();
            return response('Error Database!', 500);
        }
    }

}
